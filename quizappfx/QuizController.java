package quizappfx;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class QuizController implements Initializable {
 
    @FXML
    private Label questionText;

    @FXML
    private CheckBox optionA;

    @FXML
    private CheckBox optionB;

    @FXML
    private CheckBox optionC;

    @FXML
    private CheckBox optionD;

    @FXML
    private Label scoreCountLabel;
    
    @FXML
    private Button nextButton;

    
    @FXML
    private Label questionNum;
    
    @FXML
    void optionAClicked(ActionEvent event) {
        optionADisable(true);
    }

    @FXML
    void optionBClicked(ActionEvent event) {
        optionBDisable(true);

    }

    @FXML
    void optionCClicked(ActionEvent event) {
        optionCDisable(true);
    }

    @FXML
    void optionDClicked(ActionEvent event) {
        optionDDisable(true);
    }
    
    @FXML
    void nextButtonClicked(ActionEvent event) {
        
        if ("Finish".equals(nextButton.getText())) {
            System.exit(0);
        }
        
        String yourAnswer="";
        
        if(optionA.isSelected()){ 
            yourAnswer=optionA.getText(); 
            optionA.setSelected(false);
            optionADisable(false);
        }
        else if(optionB.isSelected()){ 
            yourAnswer=optionB.getText(); 
            optionB.setSelected(false);
            optionBDisable(false);
        }
        else if(optionC.isSelected()){ 
            yourAnswer=optionC.getText(); 
            optionC.setSelected(false);
            optionCDisable(false);
        }
        else if(optionD.isSelected()){
            yourAnswer=optionD.getText(); 
            optionD.setSelected(false); 
            optionDDisable(false);
        }
        
        quiz.checkAnswer(yourAnswer);

        // show next
        showNextQuestion();
    }

    ArrayList<Question> questions;
    Quiz quiz;
    int score;
    
    @Override
    public void initialize(URL url, ResourceBundle rb){
        quiz = new Quiz();
        quiz.loadData();
        questions = quiz.getQuestions();
        showNextQuestion();
    }
    
    public void showNextQuestion() {
        
        scoreCountLabel.setText("" + quiz.getCorrectCount() + "/" + quiz.getTotalCount());
        
        if (questions.size() == quiz.getQuestionIndex()) {
            nextButton.setText("Finish");
            clearBoard();
            return;
        }
        
        int questionIndex = quiz.getQuestionIndex();
        Question question = questions.get(questionIndex);
        
        questionText.setText(question.getQuestion());     
        optionA.setText(question.getOption().optionA);
        optionB.setText(question.getOption().optionB);
        optionC.setText(question.getOption().optionC);
        optionD.setText(question.getOption().optionD);
        
        questionNum.setText("" + (quiz.getQuestionIndex()+1)  + "" );
    }
    
    
    void optionADisable(boolean val){
        optionB.setDisable(val);
        optionC.setDisable(val);
        optionD.setDisable(val);    
    }
    
    void optionBDisable(boolean val){
        optionA.setDisable(val);
        optionC.setDisable(val);
        optionD.setDisable(val);   
    }
    
    void optionCDisable(boolean val){
        optionA.setDisable(val);
        optionB.setDisable(val);
        optionD.setDisable(val); 
    }
    
    void optionDDisable(boolean val){
        optionA.setDisable(val);
        optionB.setDisable(val);
        optionC.setDisable(val); 
    }
    
    void clearBoard(){
        questionText.setText("");     
        optionA.setText("");
        optionB.setText("");
        optionC.setText("");
        optionD.setText("");
        optionA.setVisible(false);
        optionB.setVisible(false);
        optionC.setVisible(false);
        optionD.setVisible(false);
    }
}
