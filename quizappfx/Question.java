package quizappfx;

public class Question {
    private String question;
    private Option option;
    private Answer answer;
    
    public Question(String question, Option option, Answer answer){
        this.question=question;
        this.option = option;
        this.answer = answer;
    }
   
    public void doJob() {
//        int rnd = 1 + (int)(Math.random() * (4));
//        
//        switch (rnd) {
//         } 
    }
    
    public String getQuestion() {
        return question;
    }
    
    public Option getOption(){
        return this.option;
    }
    
    public String getAnswer(){
        return this.answer.getAnswer();
    }

}
