package quizappfx;

import java.util.ArrayList;

public class Quiz {
    
    ArrayList<Question> questions;
    static int totalCount = 0;
    static int questionIndex = 0;
    static int correctCount = 0;
    
    public ArrayList<Question> getQuestions() {
        return questions;
    }
    
    private ArrayList<Question>  makeQuestions(){
        Question q1 = new Question("How much will your bill be if your pasta cost $6.75 and your two drinks were $3.85 each?",new Option("16", "14,45", "12.5", "18,3"), new Answer("14,45"));
        Question q2 = new Question("If today is Monday, what is the day after tomorrow?", new Option("Wednesday", "Tuesday", "Friday", "Thursday"), new Answer("Wednesday"));
        Question q3 = new Question("One movie ticket costs $1.20, how much you have to pay for 10 tickets?", new Option("12", "10", "14.2", "22"), new Answer("12"));
        Question q4 = new Question("What time will you arrive if your train departs at 9:45am and travels for 6 hours and 35 minutes?", new Option("18:00", "16", "16:20", "12:30"), new Answer("16:20"));
        Question q5 = new Question("Assuming that one human year equals seven dog years, and you are 28, how old are you in dog years?", new Option("4", "214", "196", "144"), new Answer("196"));

        questions = new ArrayList<Question> ();
        questions.add(q1);
        questions.add(q2);
        questions.add(q3);
        questions.add(q4);
        questions.add(q5);
          
        totalCount= questions.size();
        return questions;
    }
    
    public void loadData(){
        makeQuestions();
    }
    
    public int getQuestionIndex(){
        return questionIndex;
    }
    
    public int getCorrectCount(){
        return correctCount;
    }
    
    public int getTotalCount(){
        return totalCount;
    }
        
    public void checkAnswer(String yourAnswer) {
        if(questionIndex >= questions.size()){
            return;
        }
        
        Question currentQuestion = questions.get(questionIndex);
        
        if("".equals(yourAnswer)){
            
        }
        else if(currentQuestion.getAnswer().equals(yourAnswer)){
            correctCount++;
        }
        questionIndex++;
    }
    
}
